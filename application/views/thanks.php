<!DOCTYPE html>
<html lang="id-ID">

<head>

	<!-- Meta
	============================================= -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, intial-scale=1, max-scale=1">

    <meta name="author" content="Bondan Noviada">
    <!-- description -->
    <meta name="description" content="Ruang Edukasi Diskusi adalah salah satu program dari Red Group (PT Guna Artha Kencana) dalam memberikan pendidikan dan pengetahuan kepada pebisnis tentang IT, Pajak, dan Manajemen sehingga nantinya dapat menjadi perusahaan yang profesional dibidangnya.">
    <!-- keywords -->
    <meta name="keywords" content="charity, red, ruang diskusi, ruang edukasi, ruang edukasi dan diskusi, curhat, bisnis, it, pajak, manajemen">

	<!-- Stylesheets
	============================================= -->
	<link href="<?php echo base_url();?>assets/css/css-assets.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700i,700" rel="stylesheet">

	<!-- Favicon
	============================================= -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/general-elements/favicon/favicon.png">
	<link rel="apple-touch-icon" href="<?php echo base_url();?>assets/images/general-elements/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/images/general-elements/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/images/general-elements/favicon/apple-touch-icon-114x114.png">

	<!-- Title
	============================================= -->
	<title>R.E.D Ruang Edukasi & Diskusi</title>
</head>

<body>
	
	<div id="scroll-progress"><div class="scroll-progress"><span class="scroll-percent"></span></div></div>

	<!-- Document Full Container
	============================================= -->
	<div id="full-container">

	

		<!-- Content
		============================================= -->
		<section id="content">

			<div id="content-wrap">

				
				<!-- === CTA Title 1 =========== -->
				<div id="cta-title-1" class="flat-section">
				
					<div class="section-content">
				
						<div class="container">
							<div class="row">
								<div class="col-md-12 text-center">
				
				
									<h1>WhatsApp akan diarahkan oleh Sistem<Br />
										Agar Kami dapat segera menghubungi Anda untuk tahap selanjutnya. <br> <span class="colored">Terimakasih atas partisipasi Anda.</span></h1>
                                    <br>    
                                    <a href="<?php echo base_url();?>"> Klik Disini Kembali ke halaman sebelumnya
									<?php
									
									?></a>  		
									<?php
									//$text = "Hi Saya ".$nama_lengkap." dari ".$nama_perusahaan." (".$bidang_usaha.") saya tertarik dengan program Ruang Edukasi dan Diskusi dari R.E.D Group di bidang (".$minats.")";
									$text = "Hi,%0D%0ASaya ".$nama_lengkap."%0D%0ADari ".$nama_perusahaan." (".$bidang_usaha.").%0D%0ASaya tertarik dengan program Ruang Edukasi dan Diskusi dari Red Group%0D%0Adengan topik:%0D%0A".$minat_whatsapp."Terima Kasih";
									//$encoded_text = urlencode($text);
									$encoded_text = str_replace(" ","%20",$text);
									$encoded_text = str_replace(" ","%20",$text);
									//echo $encoded_text;
									?>


									
				
                                </div><!-- .col-md-12 end -->
                                
							</div><!-- .row end -->
						</div><!-- .container end -->
				
					</div><!-- .section-content end -->
				
				</div><!-- .flat-section end -->

			</div><!-- #content-wrap -->

		</section><!-- #content end -->



	

	</div><!-- #full-container end -->
	

	<a class="scroll-top-icon scroll-top" href="#"><i class="fa fa-angle-up"></i></a>

	<!-- External JavaScripts
	============================================= -->
	<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url();?>assets/js/jRespond.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.fitvids.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.stellar.js"></script>
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.mb.YTPlayer.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.ajaxchimp.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/simple-scrollbar.min.js"></script>
	<script src='<?php echo base_url();?>assets/js/functions.js'></script>
	


	<script type="text/javascript">
		// Get the modal
		var modal = document.getElementById("myModal");

		// Get the button that opens the modal
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];
		
		// When the user clicks on the button, open the modal
		btn.onclick = function() {
		modal.style.display = "block";
		}

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		modal.style.display = "none";
		}

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
		}

		function foo() {
		modal.style.display = "block";
		return true;
		}


		

	</script>

	<script>
	window.onload = function() {
		console.log("load");
		location.href = "https://api.whatsapp.com/send?phone=6281999747478&text=<?php echo $encoded_text;?>";
		};
	</script>

</body>
</html>
