<!DOCTYPE html> 
<html> 
  
<head> 
    <!-- Stylesheets
	============================================= -->
	<link href="<?php echo base_url();?>assets/css/css-assets.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700i,700" rel="stylesheet">
    <style> 
        .main { 
            display: block; 
            position: relative; 
            padding-left: 45px; 
            margin-bottom: 15px; 
            cursor: pointer; 
            font-size: 20px; 
        } 
          
        /* Hide the default checkbox */ 
        input[type=checkbox] { 
            visibility: hidden; 
        } 
          
        /* Creating a custom checkbox 
        based on demand */ 
        .geekmark { 
            position: absolute; 
            top: 0; 
            left: 0; 
            height: 25px; 
            width: 25px; 
            background-color: black; 
        } 
          
        /* Specify the background color to be 
        shown when hovering over checkbox */ 
        .main:hover input ~ .geekmark { 
            background-color: yellow; 
        } 
          
        /* Specify the background color to be 
        shown when checkbox is active */ 
        .main input:active ~ .geekmark { 
            background-color: red; 
        } 
          
        /* Specify the background color to be 
        shown when checkbox is checked */ 
        .main input:checked ~ .geekmark { 
            background-color: green; 
        } 
          
        /* Checkmark to be shown in checkbox */ 
        /* It is not be shown when not checked */ 
        .geekmark:after { 
            content: ""; 
            position: absolute; 
            display: none; 
        } 
          
        /* Display checkmark when checked */ 
        .main input:checked ~ .geekmark:after { 
            display: block; 
        } 
          
        /* Styling the checkmark using webkit */ 
        /* Rotated the rectangle by 45 degree and  
        showing only two border to make it look 
        like a tickmark */ 
        .main .geekmark:after { 
            left: 8px; 
            bottom: 5px; 
            width: 6px; 
            height: 12px; 
            border: solid white; 
            border-width: 0 4px 4px 0; 
            -webkit-transform: rotate(45deg); 
            -ms-transform: rotate(45deg); 
            transform: rotate(45deg); 
        } 
    </style> 
</head> 
  
<body> 
      
    <h1 style="color:green;"> 
        Best Computer Science Platform 
    </h1> 
      
    <label class="main">CodeX 
        <input type="checkbox"> 
        <span class="geekmark"></span> 
    </label> 
      
    <label class="main">GeeksforGeeks 
        <input type="checkbox" checked="checked"> 
        <span class="geekmark"></span> 
    </label> 
      
    <label class="main">CodeY 
        <input type="checkbox"> 
        <span class="geekmark"></span> 
    </label> 
</body> 
<!-- External JavaScripts
	============================================= -->
	<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url();?>assets/js/jRespond.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.fitvids.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.stellar.js"></script>
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.mb.YTPlayer.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.ajaxchimp.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/simple-scrollbar.min.js"></script>
	<script src='<?php echo base_url();?>assets/js/functions.js'></script>
</html>    