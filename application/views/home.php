<!DOCTYPE html>
<html lang="id-ID">

<head>

	<!-- Meta
	============================================= -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, intial-scale=1, max-scale=1">

	<meta name="author" content="Bondan Noviada">
	<!-- description -->
	<meta name="description"
		  content="Ruang Edukasi Diskusi adalah salah satu program dari Red Group (PT Guna Artha Kencana) dalam memberikan pendidikan dan pengetahuan kepada pebisnis tentang IT, Pajak, dan Manajemen sehingga nantinya dapat menjadi perusahaan yang profesional dibidangnya.">
	<!-- keywords -->
	<meta name="keywords"
		  content="charity, red, ruang diskusi, ruang edukasi, ruang edukasi dan diskusi, curhat, bisnis, it, pajak, manajemen">

	<!-- Stylesheets
	============================================= -->
	<link href="<?php echo base_url(); ?>assets/css/css-assets.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700i,700" rel="stylesheet">

	<style>
		.main {
			display: block;
			position: relative;
			padding-left: 45px;
			margin-bottom: -70px !important;
			cursor: pointer;
			font-size: 15px;
		}

		/* Hide the default checkbox */
		input[type=checkbox] {
			visibility: hidden;
		}

		/* Creating a custom checkbox
		based on demand */
		.geekmark {
			margin-left: 5px;
			margin-top: 5px;
			position: absolute;
			top: 0;
			left: 0;
			height: 25px;
			width: 25px;
			background-color: black;
		}

		/* Specify the background color to be
		shown when hovering over checkbox */
		.main:hover input ~ .geekmark {
			background-color: #b71c1c;
		}

		/* Specify the background color to be
		shown when checkbox is active */
		.main input:active ~ .geekmark {
			background-color: red;
		}

		/* Specify the background color to be
		shown when checkbox is checked */
		.main input:checked ~ .geekmark {
			background-color: red;
		}

		/* Checkmark to be shown in checkbox */
		/* It is not be shown when not checked */
		.geekmark:after {
			content: "";
			position: absolute;
			display: none;
		}

		/* Display checkmark when checked */
		.main input:checked ~ .geekmark:after {
			display: block;
		}

		/* Styling the checkmark using webkit */
		/* Rotated the rectangle by 45 degree and
		showing only two border to make it look
		like a tickmark */
		.main .geekmark:after {
			left: 8px;
			bottom: 5px;
			width: 6px;
			height: 12px;
			border: solid white;
			border-width: 0 4px 4px 0;
			-webkit-transform: rotate(45deg);
			-ms-transform: rotate(45deg);
			transform: rotate(45deg);
		}
	</style>

	<!-- Favicon
	============================================= -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/general-elements/favicon/favicon.png">
	<link rel="apple-touch-icon"
		  href="<?php echo base_url(); ?>assets/images/general-elements/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72"
		  href="<?php echo base_url(); ?>assets/images/general-elements/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114"
		  href="<?php echo base_url(); ?>assets/images/general-elements/favicon/apple-touch-icon-114x114.png">

	<!-- Title
	============================================= -->
	<title>R.E.D Ruang Edukasi & Diskusi</title>
</head>

<body>

<div id="scroll-progress">
	<div class="scroll-progress"><span class="scroll-percent"></span></div>
</div>

<!-- Document Full Container
============================================= -->
<div id="full-container">


	<!-- Header
	============================================= -->
	<header id="header">
		<!-- The Modal -->
		<div id="myModal" class="modal">

			<!-- Modal content -->
			<div class="modal-content">
				<h1 align="center"><span class="colored">Mohon Menunggu<br /> Sistem Sedang Memproses . . .</span></h1>
			</div>

		</div>

		<div id="header-bar-1" class="header-bar">

			<div class="header-bar-wrap">

				<div class="container">
					<div class="row">
						<div class="col-md-12">

							<div class="hb-content">
								<a class="logo logo-header" href="https://www.redgroup.id/">
									<img class="logo-red-group"
										 src="<?php echo base_url(); ?>assets/images/logo-red-group.png" alt="">

								</a><!-- .logo end -->
								<div class="hb-meta">
								</div><!-- .hb-meta end -->
							</div><!-- .hb-content end -->

						</div><!-- .col-md-12 end -->
					</div><!-- .row end -->
				</div><!-- .container end -->

			</div><!-- .header-bar-wrap -->

		</div><!-- #header-bar-1 end -->

	</header><!-- #header end -->

	<!-- Banner
	============================================= -->
	<section id="banner">

		<div class="banner-parallax" data-banner-height="800">
			<img src="<?php echo base_url(); ?>assets/images/red-charity.jpg" alt="">
			<div class="overlay-colored color-bg-gradient opacity-90"></div><!-- .overlay-colored end -->
			<div class="slide-content">

				<div class="container">
					<div class="row">
						<div class="col-md-7">
						

							<div class="banner-center-box text-white md-text-center">
								<!-- <h1 style="color:#000000">
									R.E.D.  <br> (Ruang <br> Edukasi <br> & Diskusi)
								</h1> -->
								<div class="videosss">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/Gi-SnpIaTNU?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
								<img class="svg logozz"
									 src="<?php echo base_url(); ?>assets/images/logo-ruang-edukasi-diskusi.png" alt="">
								<div style="color:#000000; margin-bottom:-50px" class="description">
									<p class="textkecilzz">Sebuah ruang untuk berdiskusi <br> yang akan menghasilkan <b>SOLUSI</b>
										dan <b>EDUKASI.</b></p>
								</div>


								<!-- <a class="btn-video lightbox-iframe mt-40" href="https://www.youtube.com/watch?v=hRUrHfbehlY">
									<i class="fa fa-play"></i>
									<span style="color:#000000" class="title">Sambutan dari CEO R.E.D Group <br>Inspirasi yang bisa menyapa anda . .</span>
								</a>.video-btn end -->
							</div><!-- .banner-center-box end -->

						</div><!-- .col-md-7 end -->
						<div class="col-md-5">

							<div class="banner-center-box text-center text-white">
								<div class="cta-subscribe cta-subscribe-2 box-form">
									<div class="box-title text-white">
										<h3 class="title">Daftar Di Sini</h3>
										<!-- <p>Program Charity khusus UMKM yang ingin bertumbuh dan berkembang</p> -->
										<img class="svg"
											 src="<?php echo base_url(); ?>assets/images/general-elements/section-separators/rounded.svg"
											 alt="">
									</div><!-- .box-title end -->
									<div class="box-content">
										<?php echo form_open_multipart('home/send	', 'class="form-inline form-submit"'); ?>
										<div class="cs-notifications">
											<div class="cs-notifications-content"></div>
										</div><!-- .cs-notifications end -->
										<div class="form-group">
											<label> <span style="color:red">*</span> Email</label>
											<input type="email" name="email" required id="email" class="form-control"
												   placeholder="">
										</div><!-- .form-group end -->
										<div class="form-group">
											<label> <span style="color:red">*</span> Nama Lengkap</label>
											<input type="text" name="nama_lengkap" required id="nama_lengkap"
												   class="form-control" placeholder="">
										</div><!-- .form-group end -->
										<div class="form-group">
											<label><span style="color:red">*</span>Nama Usaha/Perusahaan</label>
											<input type="text" name="nama_perusahaan" required id="nama_perusahaan"
												   class="form-control" placeholder="">
										</div><!-- .form-group end -->
										<div class="form-group">
											<label><span style="color:red">*</span> Bidang Usaha</label>
											<input type="text" name="bidang_usaha" required id="bidang_usaha"
												   class="form-control" placeholder="">
										</div><!-- .form-group end -->
										<div class="form-group">
											<label><span style="color:red">*</span> No HP (Whatsapp)</label>
											<input type="text" name="no_hp" id="no_hp" required class="form-control"
												   placeholder="">
										</div><!-- .form-group end -->
										<div class="form-group">

											<label>Topik Diskusi yang Diminati (Dapat mencentang lebih dari satu
												pilihan)</label>
											<div class="containerzz">
												<br>
												<label class="main"><p style="text-align: left">Perpajakan</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Perpajakan" type="checkbox">
													<span class="geekmark"></span>
												</label>

												<label class="main"><p style="text-align: left">Keuangan Perusahaan</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Keuangan Perusahaan" type="checkbox">
													<span class="geekmark"></span>
												</label>

												<label class="main"><p style="text-align: left">Pengelolaan Sumber Daya
														Manusia</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Pengelolaan Sumber Daya Manusia" type="checkbox">
													<span class="geekmark"></span>
												</label>

												<label class="main"><p style="text-align: left">Manajemen Perusahaan</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Manajemen Perusahaan" type="checkbox">
													<span class="geekmark"></span>
												</label>
												<label class="main"><p style="text-align: left">SOP Perusahaan</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="SOP Perusahaan" type="checkbox">
													<span class="geekmark"></span>
												</label>
												<label class="main"><p style="text-align: left">IT (Teknologi Informasi)
														Perusahaan</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="IT (Teknologi Informasi) Perusahaan" type="checkbox">
													<span class="geekmark"></span>
												</label>
												<label class="main"><p style="text-align: left">Online Branding &
														Marketing</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Online Branding & Marketing" type="checkbox">
													<span class="geekmark"></span>
												</label>
												<label class="main"><p style="text-align: left">Legalitas Perusahaan</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Legalitas Perusahaan" type="checkbox">
													<span class="geekmark"></span>
												</label>
												<label class="main"><p style="text-align: left">Investasi Usaha</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Investasi Usaha" type="checkbox">
													<span class="geekmark"></span>
												</label>
												<label class="main"><p style="text-align: left">Analisa Investasi</p>
													<input name="topik_diskusi[]" id="topik_diskusi[]"
														   value="Investasi Usaha" type="checkbox">
													<span class="geekmark"></span>
												</label>

											</div><!-- .form-group end -->
										</div>
										<div class="form-group">
											<label>Pesan Singkat Kepada Kami</label>
											<textarea type="text" name="pesan_singkat" id="pesan_singkat" required
													  class="form-control"></textarea>
										</div><!-- .form-group end -->
										<div class="form-group">
											<input type="submit" class="form-control" onclick="//return foo();"
												   value="Kirim">
										</div><!-- .form-group end -->
										</form><!-- #form-cta-subscribe-2 end -->
									</div><!-- .box-content end -->
								</div><!-- .box-form end -->
							</div><!-- .banner-center-box end -->

						</div><!-- .col-md-5 end -->
					</div><!-- .row end -->
				</div><!-- .container end -->

			</div><!-- .slide-content end -->
			<div class="section-separator wave-1 bottom">
				<div class="ss-content">
					<img class="svg"
						 src="<?php echo base_url(); ?>assets/images/general-elements/section-separators/wave-1.svg"
						 alt="">
				</div><!-- .ss-content -->
			</div><!-- .section-separator -->
		</div><!-- .banner-parallax end -->

	</section><!-- #banner end -->

	<!-- Content
	============================================= -->
	<section id="content">

		<div id="content-wrap">


			<!-- === CTA Title 1 =========== -->
			<div id="cta-title-1" class="flat-section">

				<div class="section-content">

					<div class="container">
						<div class="row">
							<div class="col-md-12 text-center colzz">


								<h2 class="try">Jangan sampai melewatkan kesempatan berharga ini <br><br><span
										class="colored">"Sharing is Caring"</span></h2>


							</div><!-- .col-md-12 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->

				</div><!-- .section-content end -->

			</div><!-- .flat-section end -->

		</div><!-- #content-wrap -->

	</section><!-- #content end -->

	<!-- Footer
	============================================= -->
	<footer class="footerzz" id="footer">

		<div id="footer-bar-1" class="footer-bar">

			<div class="footer-bar-wrap">

				<div class="container">
					<div class="row">
						<div class="col-md-12">

							<div class="fb-row">
								<div class="col-sm-4">

									<div class="box-info box-info-2 mb-sm-50">
										<div class="box-content">
											<h4>Our Address</h4>
											<p>
												<a target="_blank" href="https://www.google.com/maps/place/PT+Guna+Artha+kencana+%22Red+Consulting%22/@-8.6404702,115.2282206,15z/data=!4m2!3m1!1s0x0:0x28f52169c1dbb072?sa=X&ved=2ahUKEwjst7WTvv3pAhWV83MBHVs7DdUQ_BIwCnoECBEQCA">
													Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239
												</a>
											</p>
										</div><!-- .box-content end -->
									</div><!-- .box-info end -->

								</div><!-- .col-sm-4 end -->
								<div class="col-sm-4">

									<div class="box-info box-info-2 mb-sm-50">
										<div class="box-content">
											<h4>Our Phones</h4>
											<p>
												<a href="telp:+62 819-9974-7478">+62 819-9974-7478 (WA Only)</a><br />
												<a href="telp:+62 822-4707-7326">+62 822-4707-7326 (WA Only)</a>
											</p>
										</div><!-- .box-content end -->
									</div><!-- .box-info end -->

								</div><!-- .col-sm-4 end -->
								<div class="col-sm-4">

									<div class="box-info box-info-2">
										<div class="box-content">
											<h4>Email Contact</h4>
											<p>
												<a href="mailto:support@redgroup.id">support@redgroup.id</a>
											</p>
										</div><!-- .box-content end -->
									</div><!-- .box-info end -->

								</div><!-- .col-sm-4 end -->
							</div><!-- .fb-row end -->

						</div><!-- .col-md-12 end -->
					</div><!-- .row end -->
				</div><!-- .container end -->

			</div><!-- .footer-bar-wrap -->

		</div><!-- #footer-bar-1 end -->
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
			 aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						...
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>

		<div id="footer-bar-2" class="footer-bar">

			<div class="footer-bar-wrap">

				<div class="container">
					<div class="row">
						<div class="col-md-12">

							<div class="fb-row">
								<div class="copyrights-message">2020 © <a href="http://www.redsystem.id"
																		  target="_blank"><span class="colored">Red Group</span></a>.
									All Rights Reserved.
								</div>
								<ul class="social-icons animated x4 grey hover-colorful icon-only">
									<li><a class="si-facebook"
										   href="https://web.facebook.com/RED-Consulting-102209444767489"><i
												class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li>
									<li><a class="si-instagramorange"
										   href="https://www.instagram.com/inforedconsulting/"><i
												class="fa fa-instagram"></i><i class="fa fa-instagram"></i></a></li>
								</ul><!-- .social-icons end -->
							</div><!-- .fb-row end -->

						</div><!-- .col-md-12 end -->
					</div><!-- .row end -->
				</div><!-- .container end -->

			</div><!-- .footer-bar-wrap -->

		</div><!-- #footer-bar-2 end -->

	</footer><!-- #footer end -->


</div><!-- #full-container end -->


<a class="scroll-top-icon scroll-top" href="#"><i class="fa fa-angle-up"></i></a>

<!-- External JavaScripts
============================================= -->
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jRespond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.fitvids.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.stellar.js"></script>
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.mb.YTPlayer.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
<script src='<?php echo base_url(); ?>assets/js/functions.js'></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $('.form-submit').submit(function (e) {
            e.preventDefault();

            // Get the modal
            var modal = document.getElementById("myModal");

            modal.style.display = "block";
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize(),
                success: function () {
                    modal.style.display = "none";

                    Swal.fire({
                        title: 'Registrasi Berhasil',
                        text: "Silahkan klik Okay di bawah ini untuk masuk ke tahap selanjutnya.",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Okay'
                    }).then((result) => {
                        if (result.value) {
                            window.location.href = "<?php echo base_url('home/thanks') ?>";
                        }
                    });


                }
            });

            return false;
        });
    });

    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on the button, open the modal
    // btn.onclick = function () {
    //     modal.style.display = "block";
    // }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    function foo() {
        modal.style.display = "block";
        return true;
    }

</script>

</body>
</html>
