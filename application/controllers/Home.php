<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;

class Home extends CI_Controller
{
	public function index()
	{
		$this->load->view('home');
	}

	public function send()
	{

		$email = $this->input->post('email');
		$data['email'] = $email;
		//echo $email.'<br>';
		$nama_lengkap = $this->input->post('nama_lengkap');
		$data['nama_lengkap'] = $nama_lengkap;
		//echo $nama_lengkap.'<br>';
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$data['nama_perusahaan'] = $nama_perusahaan;
		//echo $nama_perusahaan.'<br>';
		$bidang_usaha = $this->input->post('bidang_usaha');
		$data['bidang_usaha'] = $bidang_usaha;
		//echo $bidang_usaha.'<br>';
		$no_hp = $this->input->post('no_hp');
		$data['no_hp'] = $no_hp;
		//echo $no_hp.'<br>';

		$name = $this->input->post('topik_diskusi');
		$minat = "";
		$minats = "";
		$minat_whatsapp = "";
		$no = 1;
		foreach ($name as $color) {
			$minat = $minat . '<br>' . $no . ". " . $color;
			$minats = $minats . ', ' . $color;
			//echo $color."<br />";

			$minat_whatsapp .= $no . ". " . $color . '%0D%0A';

			$no = $no + 1;
		}
		$data['minat'] = $minat;
		$data['minats'] = $minats;
		$pesan_singkat = $this->input->post('pesan_singkat');
		$data['pesan_singkat'] = $pesan_singkat;
		$data['minat_whatsapp'] = $minat_whatsapp;
		//echo $pesan_singkat.'<br>';

		$this->session->set_userdata($data);

		require APPPATH . 'libraries/Exception.php';
		require APPPATH . 'libraries/PHPMailer.php';
		require APPPATH . 'libraries/SMTP.php';


		// PHPMailer object
		$response = false;
		$mail_user = new PHPMailer();
		$mail_admin = new PHPMailer();


//          // SMTP configuration
//          $mail->isSMTP();
//          $mail->Host     = 'smtp.gmail.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
//          $mail->SMTPAuth = true;
//          $mail->Username = 'redsystem.team@gmail.com'; // user email
//          $mail->Password = 'sukses69TEAM'; // password email
//          //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
//          $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
//          $mail->Port     = 587;
//          $mail->SMTPDebug = 0;
//
//          $mail->setFrom('redsystem.team@gmail.com','Red Group - Ruang Edukasi & Diskusi'); // user email
//          //$mail->addReplyTo('noviadarkbondan@gmail.com', 'Bondan Noviada'); //user email
//
//          // Add a recipient
//          $mail->addAddress($email); //email tujuan pengiriman email
//
//          // Email subject
//          $mail->Subject = 'Red Group - Ruang Edukasi & Diskusi'; //subject email
//
//          // Set email format to HTML
//          $mail->isHTML(true);


		$host = 'mail.redgroup.id';
		$username = 'support@redgroup.id';
		$password = 'Support123!@#';

		// Email body content
		$mailContentAdmin =
			"Nama Lengkap : " . $nama_lengkap . "<br>" .
			"Email : " . $email . "<br>" .
			"Nama Perusahaan : " . $nama_perusahaan . "<br>" .
			"Bidang Usaha : " . $bidang_usaha . "<br>" .
			"No HP/WhatsApp : " . $no_hp . "<br>" .
			"Minat Topik Diskusi : " . $minat . "<br>" .
			"Pesan Singkat : " . $pesan_singkat . "<br>"; // isi email
//          $mail->Body = $mailContent;

		$mailContentUser = '
			Hi, ' . $nama_lengkap . ',<br />
			Selamat datang di program RED - Ruang Edukasi & Diskusi,<br />
			sebuah ruang untuk berbagi informasi dan berdiskusi yang akan menghasilkan solusi dan edukasi.<br />
			Team kami akan segera menghubungi Anda untuk tahap selanjutnya.<br />
			Semoga program ini bisa bermanfaat untuk pertumbuhan bisnis Anda ke depannya.<br />
			Terima kasih.<br />
			<Br />
			Warm regards,<br />
			Rajya Yodie<br />
			CEO R.E.D. GROUP
		';


		/**
		 * Send to User
		 */

		try {
			$mail_user->IsSMTP();
			$mail_user->SMTPSecure = "ssl";
			$mail_user->Host = $host; //hostname masing-masing provider email
			$mail_user->SMTPDebug = 2;
			$mail_user->SMTPDebug = FALSE;
			$mail_user->do_debug = 0;
			$mail_user->Port = 465;
			$mail_user->SMTPAuth = true;
			$mail_user->Username = $username; //user email
			$mail_user->Password = $password; //password email
			$mail_user->SetFrom("support@redgroup.id ", 'Red Group'); //set email pengirim
			$mail_user->Subject = 'Register - Ruang Edukasi & Diskusi'; //subyek email
			$mail_user->AddAddress($email, $nama_lengkap); //tujuan email
			$mail_user->MsgHTML($mailContentUser);
//			if ($file) {
//				$mail->addAttachment("upload/images/" . $file);
//			}
			$mail_user->Send();
			//echo "Message has been sent";
		} catch (phpmailerException $e) {
			echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (\Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}

		/**
		 * Send to Admin
		 */
		try {
			$mail_admin->IsSMTP();
			$mail_admin->SMTPSecure = "ssl";
			$mail_admin->Host = $host; //hostname masing-masing provider email
			$mail_admin->SMTPDebug = 2;
			$mail_admin->SMTPDebug = FALSE;
			$mail_admin->do_debug = 0;
			$mail_admin->Port = 465;
			$mail_admin->SMTPAuth = true;
			$mail_admin->Username = $username; //user email
			$mail_admin->Password = $password; //password email
			$mail_admin->SetFrom("support@redgroup.id ", 'Red Group'); //set email pengirim
			$mail_admin->Subject = 'New Register - Ruang Edukasi & Diskusi'; //subyek email
			$mail_admin->AddAddress($username, 'Red Group'); //tujuan email
			$mail_admin->MsgHTML($mailContentAdmin);
//			if ($file) {
//				$mail->addAttachment("upload/images/" . $file);
//			}
			$mail_admin->Send();
			//echo "Message has been sent";
		} catch (phpmailerException $e) {
			echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (\Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}

//		redirect('home/thanks/'.$data);
//		$this->load->view('thanks',$data);

		// Send email
//          if(!$mail->send()){
//              echo 'Message could not be sent.';
//              echo 'Mailer Error: ' . $mail->ErrorInfo;
//          }else{
//              //echo 'Message has been sent';
//              //redirect('home/thanks/'.$data);
//              //$this->load->view('thanks',$data);
//          }

	}

	public function thanks()
	{
		$data = $this->session->userdata();
		$this->load->view('thanks', $data);
	}


	public function sends()
	{
		$email = $this->input->post('email');
		echo $email . '<br>';
		$nama_lengkap = $this->input->post('nama_lengkap');
		echo $nama_lengkap . '<br>';
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		echo $nama_perusahaan . '<br>';
		$bidang_usaha = $this->input->post('bidang_usaha');
		echo $bidang_usaha . '<br>';
		$no_hp = $this->input->post('no_hp');
		echo $no_hp . '<br>';
		// optional
		// echo "You chose the following color(s): <br>";
		$name = $this->input->post('topik_diskusi');
		foreach ($name as $color) {
			echo $color . "<br />";
		}
		$pesan_singkat = $this->input->post('pesan_singkat');
		echo $pesan_singkat . '<br>';


	}


}
